<?php

if (! function_exists('str_lower'))
{
    function str_lower($str)
    {
        return Illuminate\Support\Str::lower($str);
    }
}

if (! function_exists('str_upper'))
{
    function str_upper($str)
    {
        return Illuminate\Support\Str::upper($str);
    }
}

if (! function_exists('stylesheet_url'))
{
    function stylesheet_url($file)
    {
        return asset_url($file . '.css');
    }
}

if (! function_exists('javascript_url'))
{
    function javascript_url($file)
    {
        return asset_url($file . '.js');
    }
}

if (! function_exists('image_url'))
{
    function image_url($file)
    {
        return asset_url($file . (strpos($file, '.') !== false ? '' : '.png'));
    }
}

if (! function_exists('asset_url'))
{
    function asset_url($file)
    {
        return elixir($file);
    }
}

if (! function_exists('stylesheet_link_tag'))
{
    function stylesheet_link_tag($file)
    {
        return '<link rel="stylesheet" type="text/css" href="' . stylesheet_url($file) . '">';
    }
}

if (! function_exists('javascript_include_tag'))
{
    function javascript_include_tag($file)
    {
        return '<script src="' . javascript_url($file) . '"></script>';
    }
}


if (! function_exists('page'))
{
    function page($page)
    {
        return route('page', $page);
    }
}

if (! function_exists('user'))
{
    function user($property = null)
    {
        if (Auth::guest())
        {
            return false;
        }

        if ($property === null)
        {
            return Auth::user();
        }

        return Auth::user()->$property;
    }
}

if (! function_exists('guest'))
{
    function guest()
    {
        return Auth::guest();
    }
}

if (! function_exists('path_matches'))
{
    function path_matches($str)
    {
        return starts_with(request()->path(), $str);
    }
}

if (! function_exists('url_matches'))
{
    function url_matches($str)
    {
        return starts_with(request()->uri(), $str);
    }
}

if (! function_exists('classes'))
{
    function classes($map)
    {
        $res = [];

        foreach ($map as $value => $condition)
        {
            if (is_numeric($value))
            {
                $res[] = $condition;
            }
            elseif ($condition)
            {
                $res[] = $value;
            }
        }

        return $res ? ' class="' . implode(' ', $res) . '"' : '';
    }
}

if (! function_exists('attributes'))
{
    function attributes($map)
    {
        $res = [];

        foreach ($map as $key => $value)
        {
            if (is_array($value) || is_a($value, Illuminate\Contracts\Support\Arrayable::class))
            {
                foreach ($value as $i => $item)
                {
                    $res[] = ltrim(attributes([ $key . '-' . $i => $item ]));
                }
            }
            else
            {
                if (is_numeric($key))
                {
                    $key = $value;
                }
                if ($value !== null)
                {
                    $res[] = $key . '="' . e($value) . '"';
                }
            }

        }

        return $res ? ' ' . implode(' ', $res) : '';
    }
}
