<?php

namespace Lamotivo\Bladex;

use Blade;
use Illuminate\Support\ServiceProvider as ServiceProvider;

class BladexServiceProvider extends ServiceProvider
{
    public function boot()
    {

        Blade::directive('str_lower', function($str) {
            return "<?=str_lower($str)?>\n";
        });

        Blade::directive('str_upper', function($str) {
            return "<?=str_upper($str)?>\n";
        });

        Blade::directive('stylesheet_link_tag', function($file) {
            return "<?=stylesheet_link_tag($file)?>\n";
        });

        Blade::directive('javascript_include_tag', function($file) {
            return "<?=javascript_include_tag($file)?>\n";
        });

        Blade::directive('csrf_field', function() {
            return "<?=csrf_field()?>\n";
        });

        Blade::directive('csrf_token', function() {
            return '<meta name="csrf-token" content="<?=csrf_token()?>">';
        });

        Blade::directive('nl2br', function($text) {
            return "<?=nl2br(e($text))?>\n";
        });

        Blade::directive('urlencode', function($str) {
            return "<?=e(urlencode($str))?>\n";
        });

        Blade::directive('implode', function($array) {
            return "<?=e(implode((array)$array))?>\n";
        });

        Blade::directive('datetime', function($time) {
            return "<?=with($time)->format(config('app.datetime_format', 'd.m.Y H:i'))?>\n";
        });

        Blade::directive('date', function($time) {
            return "<?=with($time)->format(config('app.date_format', 'd.m.Y'))?>\n";
        });

        /**
         * paste class attribute with defined classes:
         *
         * <li@classes('menu-item', 'active' => path_matches('/order'))><a href="/order">...</a></li>
         * <li@classes('menu-item', 'active' => path_matches('/about'))><a href="/about">...</a></li>
         *
         * =>
         *
         * <li class="menu-item active"><a href="/order">...</a></li>
         * <li class="menu-item"><a href="/about">...</a></li>
         *
         */
        Blade::directive('classes', function($options) { return "<?= classes([$options]) ?>\n"; });

        /**
         * paste attributes from an array:
         *
         * <li@attributes('class' => 'menu-item', 'data' => $item)><a href="/order">...</a></li>
         * <li@classes('menu-item', 'active' => path_matches('/about'))><a href="/about">...</a></li>
         *
         * =>
         *
         * <li class="menu-item active"><a href="/order">...</a></li>
         * <li class="menu-item"><a href="/about">...</a></li>
         *
         */
        Blade::directive('attributes', function($options) { return "<?= attributes([$options]) ?>\n"; });


        Blade::directive('user', function($options) { return "<?= user($options) ?>\n"; });
        Blade::directive('route', function($options) { return "<?= route($options) ?>\n"; });
        Blade::directive('page', function($options) { return "<?= page($options) ?>\n"; });

        Blade::directive('json', function($object) { return "<?= json_encode($object) ?>\n"; });


        Blade::directive('link_to', function($options) { return "<?=link_to($options) ?>\n"; });
        Blade::directive('link_to_route', function($options) { return "<?=link_to_route($options) ?>\n"; });
        Blade::directive('link_to_action', function($options) { return "<?=link_to_action($options) ?>\n"; });

        Blade::directive('strip', function() { return "<?php ob_start(); ?>"; });
        Blade::directive('endstrip', function() { return "<?=preg_replace('#>\\s+<#', '><', ob_get_clean()) ?>"; });


        Blade::directive('form', function($options) { return "<?=Form::open($options) ?>\n"; });
        Blade::directive('form_for', function($options) { return "<?=Form::model($options) ?>\n"; });
        Blade::directive('endform', function() { return "<?=Form::close(); ?>\n"; });

        Blade::directive('text_field', function($options) { return "<?=Form::text($options) ?>\n"; });
        Blade::directive('text_area', function($options) { return "<?=Form::textarea($options) ?>\n"; });
        Blade::directive('password_field', function($options) { return "<?=Form::password($options) ?>\n"; });
        Blade::directive('file_field', function($options) { return "<?=Form::file($options) ?>\n"; });
        Blade::directive('checkbox_field', function($options) { return "<?=Form::checkbox($options) ?>\n"; });
        Blade::directive('radio_button_field', function($options) { return "<?=Form::radio($options) ?>\n"; });
        Blade::directive('number_field', function($options) { return "<?=Form::number($options) ?>\n"; });
        Blade::directive('select', function($options) { return "<?=Form::select($options) ?>\n"; });
        Blade::directive('submit', function($options) { return "<?=Form::submit($options) ?>\n"; });
        Blade::directive('button', function($options) { return "<?=Form::button($options) ?>\n"; });

        /**
         * font-awesome icons helper
         *
         * @fa('user') => <i class="fa fa-user"></i>
         * @fa('fw', 'user') => <i class="fa fa-fw fa-user"></i>
         *
         */
        Blade::directive('fa', function($options) {
            return "<i class=\"fa fa-<?=implode(' fa-', [$options]) ?>\"></i>";
        });

        /**
         * glyphicon icons helper
         *
         * @glyphicon('cloud') => <i class="glyphicon glyphicon-cloud"></i>
         *
         */
        Blade::directive('glyphicon', function($options) {
            return "<i class=\"glyphicon glyphicon-<?=implode(' glyphicon-', [$options]) ?>\"></i>";
        });

        /**
         * custom app icons helper
         *
         * @flaticon('logo') => <i class="flaticon flaticon-logo"></i>
         *
         */
        Blade::directive('flaticon', function($options) {
            return "<i class=\"flaticon flaticon-<?=implode(' flaticon-', [$options]) ?>\"></i>";
        });


        /**
         * custom app icons helper
         *
         * @appicon('logo') => <i class="appicon appicon-logo"></i>
         *
         */
        Blade::directive('appicon', function($options) {
            return "<i class=\"appicon appicon-<?=implode(' appicon-', [$options]) ?>\"></i>";
        });

    }

    public function register()
    {
        //
    }
}
